<?php

$bd = new SQLite3(__DIR__.'/domeins.sqlite') ;
$strSQL ="
    CREATE TABLE domeins (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        domen TEXT                
    );
";
$bd->query($strSQL);

$bd = new SQLite3(__DIR__.'/ip.sqlite') ;
$strSQL ="
    CREATE TABLE ip (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        ip TEXT,
        city TEXT,
        time TEXT
    );
";
$bd->query($strSQL);