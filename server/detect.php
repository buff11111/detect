<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With');

require_once __DIR__.'/Domen.php';
require_once __DIR__.'/Logger.php';
require_once __DIR__.'/IpApi.php';

$domen = new Domen();
Logger::logSetup(__FILE__);

Logger::log('--START--');

$result = [];
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, true); //convert JSON into array
$targetServer = $input['server'];

Logger::log('$targetServer');
Logger::log($targetServer);

$type = $input['type'];
$hourStart = 4;
$hourEnd = 20;
$hour = date('H', time());
$dayOfWeek = date('w');
$allowedDaysOfWeek = [1, 5]; //нумерация дней с воскресения (0-ой день)
$refer = explode('/', $targetServer['HTTP_REFERER'])[2];
if ($domen->selectDomein($refer)) {
    $result['allowOpen'] = true;
    $result['reason'] = 'trustedHost';
    Logger::log($result);
    Logger::log('--END--');
    return print_r(json_encode($result));
}

$isBot = (preg_match('/Bot/', $targetServer['HTTP_USER_AGENT']) ||  preg_match('/bot/', $targetServer['HTTP_USER_AGENT']));
$isValidReferer = (preg_match('/google/', $targetServer['HTTP_REFERER']) ||
    preg_match('/yandex/', $targetServer['HTTP_REFERER']) ||
    preg_match('/duckduckgo/', $targetServer['HTTP_REFERER']) ||
    preg_match('/rambler/', $targetServer['HTTP_REFERER']) ||
    preg_match('/mail/', $targetServer['HTTP_REFERER']) || preg_match('YandexBot', $targetServer['HTTP_REFERER']) ||
    preg_match('/bing/', $targetServer['HTTP_REFERER']));
$isTimeLimit = $hour >= $hourStart && $hour <= $hourEnd;
$isValidDayOfWeek = in_array($dayOfWeek, $allowedDaysOfWeek);

$city = IpApi::getInfo($targetServer['REMOTE_ADDR']);
Logger::log('$city');
Logger::log($city);
if ($city == 'Moscow' && !$isBot) {
    $result['allowOpen'] = false;
    $result['reason'] = 'Moscow';
    Logger::log($result);
    Logger::log('--END--');
    return print_r(json_encode($result));
}
// var_dump($targetServer);

// проверка на мобильное устройство. с мобильного открываем в любое время, пропуская все остальные проверки
$useragent = $targetServer['HTTP_USER_AGENT'];
//if (!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
if ($type == 0) {
    if ($isBot) { //проверка на бота
        $result['allowOpen'] = true;
        $result['reason'] = 'bot';
        Logger::log($result);
        Logger::log('--END--');
        return print_r(json_encode($result));
    } elseif ($isValidReferer) { //проверка на реферера
        $result['allowOpen'] = true;
        $result['reason'] = 'referer';
        Logger::log($result);
        Logger::log('--END--');
        return print_r(json_encode($result));
    } elseif ($isValidDayOfWeek) { //проверка на день недели
        $result['allowOpen'] = true;
        $result['reason'] = 'DOW';
        Logger::log($result);
        Logger::log('--END--');
        return print_r(json_encode($result));
    } elseif (!$isTimeLimit) { //проверка на время
        $result['allowOpen'] = true;
        $result['reason'] = 'time';
        Logger::log($result);
        Logger::log('--END--');
        return print_r(json_encode($result));
    } else { //если ничего не прошло, то запрещаем доступ к сайту
        $result['allowOpen'] = false;
    }
}
//} else { //заход с мобильного устройства
//    $result['allowOpen'] = true;
//    $result['reason'] = 'mobile';
//    return json_encode($result);
//}

if ($type == 1) {
    $result['allowOpen'] = !$isTimeLimit || $isBot;
    $result['reason'] = 'custom';
}


if ($type == 2) {
    $result['allowOpen'] = ($isValidReferer || !$isTimeLimit || $isBot);
    $result['reason'] = 'custom';
}


//alcomig4.com
//alcodost.pw

if ($isValidDayOfWeek) {
   $result['allowOpen'] = true;
   $result['reason'] = 'day of week';
}

if ($type == 3) {
    $result['allowOpen'] = ($isValidReferer || $isBot);
    $result['reason'] = 'custom';
    if ($isValidReferer) {
        $result['reason'] = 'referer';
    }
}
if ($type == 4) {
    $result['allowOpen'] = false;
    $result['reason'] = 'type=4';
    if ($isBot) { //проверка на бота
        $result['allowOpen'] = true;
        $result['reason'] = 'bot type=4';
    } elseif ($isValidReferer) { //проверка на реферера
        $result['allowOpen'] = true;
        $result['reason'] = 'referer type=4';
    }
}

Logger::log($result);
Logger::log('--END--');
return print_r(json_encode($result));
