<?php

/**
 * Description of IpApi
 *
 * @author Buffon
 */
class IpApi {
    
    static $baseUrl = 'http://ip-api.com/php/';
    
    static function getInfo($ip) {
        $bd = new SQLite3(__DIR__.'/ip.sqlite');
        $bd->enableExceptions(true);
        $strSQL = "select * from ip where ip = '$ip' and time > ". (time() - 2592000);
        try {
            $city = $bd->query($strSQL)->fetchArray(SQLITE3_ASSOC);
        } catch (Exception $exc) {
            $city = true;
        }
        if (!$city) {
            try {
                $city = self::sendToServer($ip);
                $strSQL = "
                    insert or replace into ip ( 
                         id, ip, city, time
                    )
                    VALUES ( 
                        (select id from ip where ip = '$ip'),
                        '" . $ip . "', '" . $city['city'] . "',
                        '" . time() . "'
                    );
                ";
                $bd->query($strSQL);
            } catch (Exception $ex) {
                Logger::log('ошибка:'. $ex->getMessage());
            }
        }
        $bd->close();
        return isset($city['city']) ?  $city['city'] : '';
    }
    
    static function sendToServer($ip) {
        $response = self::senderGET(self::$baseUrl.$ip);
        return unserialize($response);
    }
    
    static function errors($code)
    {
        $code = (int)$code;
        $errors = array(
            301=>'Moved permanently',
            400=>'Bad request',
            401=>'Unauthorized',
            403=>'Forbidden',
            404=>'Not found',
            500=>'Internal server error',
            502=>'Bad gateway',
            503=>'Service unavailable'
        );
        try
        {
            if($code!=200 && $code!=204 && $code!=201) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            }
        }
        catch(Exception $E)
        {
            throw new Exception('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
    }
    
    static function senderGET($link, $headers=[]){
        $curl = curl_init(); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $out = curl_exec($curl); 

        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        self::errors($code);

        return $out;
    }
}
